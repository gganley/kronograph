#![feature(proc_macro_hygiene, decl_macro)]

use rocket;

mod auth;
mod entry;
mod project;
mod types;
mod user;
use rusqlite::{params, Connection, NO_PARAMS};

use crate::types::ApiStore;

#[rocket::get("/")]
fn index() -> &'static str {
    "Welcome to Kronograph!"
}

fn rocket_main() -> rocket::Rocket {
    let conn = Connection::open_in_memory().unwrap();
    conn.execute(
        "CREATE TABLE user (
email TEXT PRIMARY KEY,
secret TEXT NOT NULL
)",
        NO_PARAMS,
    )
    .unwrap(); // Yes I'm not hashing my passwords I understand, I would do this better if needed

    rocket::ignite()
        .manage(ApiStore::default())
        .mount("/", rocket::routes![index])
        .mount(
            "/task",
            rocket::routes![entry::add, entry::edit, entry::delete, entry::backfill],
        )
        .mount(
            "/project",
            rocket::routes![project::add, project::edit, project::delete, project::data],
        )
        .mount(
            "/user",
            rocket::routes![user::add, user::delete, user::information],
        )
}

fn main() {
    rocket_main().launch();
}

#[cfg(test)]
mod test {
    use super::*;
    use rocket::http::{ContentType, Status};
    use rocket::local::Client;
    use serde_json;

    #[test]
    fn hello_world() {
        let client = Client::new(rocket_main()).expect("valid rocket instance");
        let mut response = client.get("/").dispatch();
        assert_eq!(response.status(), Status::Ok);
        assert_eq!(
            response.body_string(),
            Some("Welcome to Kronograph!".into())
        );
    }

    /// This tests `user::add`
    #[test]
    fn user_add_happy() {
        let conn = Connection::open("my_db").unwrap();
        conn.execute("DROP TABLE IF EXISTS user", params![])
            .unwrap();
        conn.execute(
            "CREATE TABLE user (
email TEXT PRIMARY KEY,
secret TEXT NOT NULL
)",
            params![],
        )
        .unwrap(); // Yes I'm not hashing my passwords I understand, I would do this better if needed

        let user_object = serde_json::json!(
            {
                "email": "me@gganley.com",
                "secret": "testPass!"
            }
        );
        let client = Client::new(rocket_main()).expect("valid rocket instance");
        let mut response = client
            .post("/user/add")
            .header(ContentType::JSON)
            .body(user_object.to_string())
            .dispatch();

        assert_eq!(response.status(), Status::Ok);
        let mut select_stmt = conn
            .prepare("SELECT secret from user WHERE email='me@gganley.com'")
            .unwrap();
        assert!(select_stmt.exists(params![]).unwrap());
    }

    #[test]
    fn user_add_unhappy() {
        let conn = Connection::open("my_db").unwrap();
        conn.execute("DROP TABLE IF EXISTS user", params![])
            .unwrap();
        conn.execute(
            "CREATE TABLE user (
email TEXT PRIMARY KEY,
secret TEXT NOT NULL
)",
            params![],
        )
        .unwrap(); // Yes I'm not hashing my passwords I understand, I would do this better if needed

        let user_object = serde_json::json!(
            {
                "email": "me@gganley.com",
            }
        );
        let client = Client::new(rocket_main()).expect("valid rocket instance");
        let mut response = client
            .post("/user/add")
            .header(ContentType::JSON)
            .body(user_object.to_string())
            .dispatch();

        assert_eq!(response.status(), Status::UnprocessableEntity);
    }

    #[test]
    fn entry_add() {}

    #[test]
    fn entry_info() {}

    #[test]
    fn entry_remove() {}
}
