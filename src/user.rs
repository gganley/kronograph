use crate::types::User;
use rocket::{delete, get, post};
use rocket_contrib::json::Json;
use rusqlite::{params, Connection, Error};
use std::io;

#[post("/add", format = "application/json", data = "<form>")]
pub fn add(form: Json<User>) -> io::Result<()> {
    let conn = Connection::open("my_db").unwrap();
    match conn.execute(
        "INSERT INTO user (email, secret) VALUES (?1, ?2)",
        params![form.email, form.secret],
    ) {
        Ok(_a) => Ok(()),
        Err(_a) => panic!("Coudln't work"),
    }
}

#[delete("/delete", format = "application/json")]
pub fn delete() {}

#[get("/information")]
pub fn information() {}
